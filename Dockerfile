FROM python:3.9
COPY ./app ./app
RUN pip install -r requirements.txt
EXPOSE 8000
CMD sh app/run.sh