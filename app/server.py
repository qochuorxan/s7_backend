from fastapi import FastAPI
from .routes import main

app = FastAPI()

app.include_router(main.router)


@app.get("/")
async def index():
    return {'status': 200, 'message': 'OK'}

