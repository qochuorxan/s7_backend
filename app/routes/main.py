import json
from collections import defaultdict
from fastapi import APIRouter

router = APIRouter()

@router.get("/log_cluster/")
async def clustering(json_file='json_data', num_clusters=3):
    if num_clusters == 3:
        typeA_error = 'typeA'
        typeB_error = 'typeB'
        typeC_error = 'typeC'
        cluster_dict = defaultdict(list)
        f = open(json_file)
        data = json.load(f)
        for name_log in data:
            if typeA_error in data:
                cluster_dict[typeA_error].append(data[name_log])
            elif typeB_error in data:
                cluster_dict[typeA_error].append(data[name_log])
            elif typeC_error in data:
                cluster_dict[typeA_error].append(data[name_log])
        f.close()
        return cluster_dict
    