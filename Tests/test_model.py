import app.main as main


def test_check_clustering():
    assert (len(main.clustering(json_file='data.json', num_clustering = 3)) > 0) == True
    assert (len(main.clustering(json_file='data.json', num_clustering = 5)) > 0) == False
    assert (len(main.clustering(json_file='parse_data.json', num_clustering = 5)) > 0) == False
    assert (len(main.clustering() > 0)) == True

